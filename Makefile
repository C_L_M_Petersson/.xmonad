# {{{ GENERAL SETTINGS

all:
	-rm $(EXECUTABLE)
	make $(EXECUTABLE)

SRCDIR=src/hs/

BUILDDIR=localbuild/
HIDIR=$(BUILDDIR)hi/
ODIR=$(BUILDDIR)o/
EXECUTABLE=xmonad-x86_64-linux

MAIN=$(SRCDIR)Xmonad.hs

# }}}

# {{{ HASKELL SETTINGS

GHC=ghc --make
GHCDIRS=-odir $(ODIR) -hidir $(HIDIR) -i$(SRCDIR)
GHCOPTS=-dynamic -O2
GHCPKGS=-package monad-loops -package xmonad -package xmonad-contrib

# }}}

# {{{ COMPILATION

$(EXECUTABLE): $(MAIN) $(ODIR) $(HIDIR)
	$(GHC) -o $@ $(GHCDIRS) $(GHCOPTS) $(GHCPKGS) $<

# }}}

# {{{ ADMINISTRATIVE

%/:
	mkdir -p $@

clean:
	rm -r $(BUILDDIR)

# }}}
