#!/bin/sh

XMONAD_CONFIG=~/.xmonad/src/hs/Xmonad.hs

getVar()
{
    egrep "^$1" $XMONAD_CONFIG \
        | cut -d= -f2-         \
        | sed "s/^[ '\"]*//g; s/[ '\"]*$//g"
}

barNormalFg=$(getVar barNormalFg)
barNormalBg=$(getVar barNormalBg)
barSelectedFg=$(getVar barSelectedFg)
barSelectedBg=$(getVar barSelectedBg)
borderFg=$(getVar borderFg)
borderBg=$(getVar borderBg)

font=$(getVar font)
