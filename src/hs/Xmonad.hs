import           System.Exit
import           System.IO

import           XMonad
import           XMonad.Actions.SpawnOn
import           XMonad.Config.Desktop
import           XMonad.Hooks.DynamicLog
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.ManageHelpers
import           XMonad.Hooks.SetWMName
import           XMonad.Layout.Groups                (GroupsMessage (Modify),
                                                      group, swapDown, swapUp)
import           XMonad.Layout.Groups.Examples       (zoomRowG)
import           XMonad.Layout.Groups.Wmii           hiding (swapDown, swapUp)
import           XMonad.Layout.MessageControl
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.MultiToggle.Instances
import           XMonad.Layout.Named
import           XMonad.Layout.NoBorders
import           XMonad.Layout.Renamed
--import           XMonad.Layout.Simplest
--import           XMonad.Layout.Tabbed
import           XMonad.StackSet                     (greedyView, shift, sink)
import           XMonad.Util.EZConfig
import           XMonad.Util.Run


barNormalFg   = "'#d9d9d9'"
barNormalBg   = "'#000000'"
barSelectedFg = "'#00e600'"
barSelectedBg = "'#000000'"
borderFg      =  "#6109b5"
borderBg      =  "#cbceb5"

font          = "\"xft:Bitstream Vera Sans Mono:size=12\""
--font          = "\"xft:Terminus Monospace:size=12:antialias=true\""


defaultTerminal       = "/usr/bin/urxvt -e zsh"
defaultFilemanager    = defaultTerminal++" -c /usr/bin/vifm"
defaultNetworkmanager = defaultTerminal++" -c \"/usr/bin/sudo /usr/bin/nmtui-connect\""
defaultWebbrowser     = "qutebrowser"
defaultPrintscreen    = "mkdir -p ~/misc/images/screenshots; maim             \
                      \ ~/misc/images/screenshots/`date +%Y-%b-%e_%H:%M:%S.%N \
                      \ | sed 's/ //g'`.png"
defaultScreensaver    = "xscreensaver-command -lock"


customXmobar =
    "exec `xmobar ~/.xmonad/src/hs/Xmobarrc.hs \
        \ -f " ++ font        ++ "             \
        \ -B " ++ barNormalBg ++ "             \
        \ -F " ++ barNormalFg ++ "             \
    \`"


menu :: String -> X()
menu x = spawn $ "exec `PATH=~/bin:$PATH ~/bin/menu_"++x++"`"


customManageHook = composeAll
    [ manageSpawn
    , manageDocks
    , isFullscreen --> doFullFloat
    , manageHook def
    ]


customStartupHook = setWMName "LG3D"
    >> spawn "/usr/bin/sh ~/.config/X11/xinitrc"
    >> spawn "/usr/bin/xwallpaper --stretch .xmonad/src/images/background.jpg"
    >> spawn "/usr/bin/xscreensaver -no-splash"


customLayout = smartBorders
             $ mkToggle (NOBORDERS ?? FULL ?? EOT)
             $ avoidStruts
             $ wmii_ii ||| Full
    where
        wmii_ii = group innerLayout zoomRowG
        innerLayout = renamed [CutWordsLeft 3]
                    $ ignore NextLayout
                    $ unEscape
                    $ column ||| tabs
        column = Tall 0 (3/100) (1/2)
        tabs   = Full


keyBindings c = mkKeymap c $
    -- Launching and killing programs
    [ ( "M-<Return>"            , spawn defaultTerminal                     )

    , ( "M-C-<Return>"          , spawn defaultWebbrowser                   )
    , ( "M-S-<Return>"          , spawn defaultNetworkmanager               )
    , ( "M-M1-<Return>"         , spawn defaultFilemanager                  )

    , ( "M-C-p"                 , spawn defaultPrintscreen                  )

    , ( "M-C-s"                 , spawn defaultScreensaver                  )

    , ( "M-r"                   , menu "run"                                )

    , ( "M-c"                   , kill                                      )
    , ( "M-S-c"                 , menu "killall"                            )

    -- Audio
    , ( "<XF86AudioLowerVolume>", spawn "amixer -D pulse set Master 2%-"    )
    , ( "<XF86AudioRaiseVolume>", spawn "amixer -D pulse set Master 2%+"    )
    , ( "<XF86AudioMute>"       , spawn "amixer -D pulse set Master toggle" )

    -- Fullscreen
    , ( "M-f"                   , sendMessage $ Toggle FULL                 )

    -- Window focus
    , ( "M-h"                   , focusGroupUp                              )
    , ( "M-l"                   , focusGroupDown                            )

    , ( "M-k"                   , focusUp                                   )
    , ( "M-j"                   , focusDown                                 )

    -- Window position
    , ( "M-S-l"                 , moveToGroupDown False                     )
    , ( "M-S-h"                 , moveToGroupUp False                       )

    , ( "M-S-k"                 , sendMessage $ Modify swapUp               )
    , ( "M-S-j"                 , sendMessage $ Modify swapDown             )

    -- Stack layout
    , ( "M-<Tab>"               , groupToNextLayout                         )
    , ( "M-S-<Tab>"             , groupToNextLayout >> groupToNextLayout    )
    , ( "M-d"                   , groupToVerticalLayout                     )
    , ( "M-s"                   , groupToTabbedLayout                       )
    , ( "M-m"                   , groupToFullLayout                         )

    -- Stack size
    , ( "M-e"                   , zoomGroupIn                               )
    , ( "M-w"                   , zoomGroupOut                              )

    -- Stack position
    , ( "M-C-l"                 , swapGroupDown                             )
    , ( "M-C-h"                 , swapGroupUp                               )

    -- Toggle float
    , ("M-t"                    , withFocused $ windows . sink              )

    -- Toggle float
    , ("M-i"                    , menu "kb"                                 )

    -- Xrandr
    , ( "M-x"                   , menu "xrandr"                             )
    , ( "M-S-x"                 , menu "xrandr --all-aspect-ratios"         )

    -- Exit and restart
    , ( "M-S-q"                 , io exitSuccess                            )
    , ( "M-q"                   , broadcastMessage ReleaseResources
                                    >> restart "xmonad" True                )
    ]
    -- Workspaces
    ++ concatMap ((\w -> [ ( "M-"  ++w, windows $ greedyView w )
                         , ( "M-S-"++w, windows $ shift      w )
                         ]
                  ) . show) ([1..9]++[0])


main = do
    xmproc <- spawnPipe customXmobar

    xmonad $ desktopConfig
        { borderWidth        = 2
        , manageHook         = customManageHook
        , layoutHook         = customLayout
        , logHook            = dynamicLogWithPP xmobarPP
                               { ppOutput = hPutStrLn xmproc
                               , ppTitle  = xmobarColor "green" "" . shorten 50
                               }
        , normalBorderColor  = borderBg
        , focusedBorderColor = borderFg
        , modMask            = mod4Mask
        , terminal           = defaultTerminal
        , handleEventHook    = docksEventHook
        , startupHook        = customStartupHook
        , workspaces         = map show $ [1..9]++[0]
        , keys               = keyBindings
        }
