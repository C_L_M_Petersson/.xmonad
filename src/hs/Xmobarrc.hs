Config {
          borderColor   = "black"
        , border        = TopB
        , position      = TopW L 100
        , allDesktops   = True

        , sepChar       = "%"
        , alignSep      = "}{"
        , template      = "%StdinReader% \
                        \| %cpu% \
                        \| %memory%, %swap% \
                        \| %eth0%, %wlan0% \
                        \}{\
                        \  Keyboard: <fc=#eeee00>%keyboard%</fc> \
                        \| Volume: <fc=#00ee00>%volume_on%</fc>\
                                  \<fc=#ee0000>%volume_off%</fc> \
                        \| <fc=#ee9a00>%date%</fc> \
                        \| %battery% "

        , commands      = [
              Run Network "enp4s0"
                [   "-L"        ,"0"
                ,   "-H"        ,"32"
                ,   "--normal"  ,"yellow"
                ,   "--high"    ,"yellow"
                ] 10

            , Run Cpu
                [   "-L"        ,"3"
                ,   "-H"        ,"50"
                ,   "--normal"  ,"yellow"
                ,   "--high"    ,"yellow"
                ] 10

            , Run Memory
                [   "-t"        ,"Mem: <usedratio>%"
                ] 10

            , Run Swap
                [] 10

            , Run Com "sh" [ "-c", "~/.xmonad/src/sh/volume.sh false" ]
                "volume_on"  1
            , Run Com "sh" [ "-c", "~/.xmonad/src/sh/volume.sh true"  ]
                "volume_off" 1

            , Run Com "sh" ["-c", "~/.xmonad/src/sh/keyboard.sh"  ]
                "keyboard"   1
            , Run Battery
                [ "--template"  , "Battery: <acstatus>"
                , "--Low"       , "10"
                , "--High"      , "80"
                , "--low"       , "darkred"
                , "--normal"    , "darkorange"
                , "--high"      , "darkgreen"
                , "--"          , "-o", "<left>% (<timeleft>)"
                                , "-O", "<fc=#dAA520>Charging</fc>"
                                , "-i", "<fc=#00ee00>Charged</fc>"
                ] 50
            , Run Date "%a %b %d %Y %H:%M:%S" "date" 10
            , Run StdinReader
            ]
        }
